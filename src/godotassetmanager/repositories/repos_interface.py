import logging
from typing import Optional

from godotassetmanager import util
from godotassetmanager.assetdata import AssetData
from godotassetmanager.repositories import local
from godotassetmanager.repositories import remote

log = logging.getLogger("gam")


def get_asset_names() -> set[str]:
    """Get all available asset names."""
    return set(remote.get_asset_names() + local.get_asset_names())


def get_dependencies_data(asset_data: AssetData) -> list[AssetData]:
    """Recursively get all dependencies of an asset.

    Local repositories will be searched first, then remote.

    :param asset_data: The data of the asset to get dependencies of.
    :return: A list of all dependencies and dependency dependencies etc.
    """

    def _get_all(
        ad: AssetData, dl: Optional[list[AssetData]] = None
    ) -> list[AssetData]:
        dl = dl or []
        for dep in ad.dependencies:
            asset_name, rule = util.get_name_and_version_rule(dep)
            asset_version = get_version(asset_name, rule)
            if not asset_version:
                if rule:
                    log.error("Failed to find %s%s.", asset_name, rule)
                else:
                    log.error("Failed to find asset %s.", asset_name)
                continue
            iter_ad = get_asset_data(asset_name, asset_version)
            if iter_ad and iter_ad not in dl:
                dl.append(iter_ad)
            if iter_ad and iter_ad.dependencies:
                _get_all(iter_ad, dl)
        return dl

    all_dependencies = _get_all(asset_data)
    duplicates = [
        [a for a in all_dependencies if a.name == asset.name]
        for asset in all_dependencies
    ]
    # TODO Test this.
    for asset_list in duplicates:
        if len(asset_list) > 1:
            asset_list.sort(key=lambda a: a.version)
            # Remove all but the latest version.
            [all_dependencies.remove(it) for it in asset_list[:-1]]
    return all_dependencies


def get_asset_data(asset_name: str, asset_version: str) -> Optional[AssetData]:
    """Get the asset data of an asset.

    `   Local repositories will be searched first, then remote.

        :param asset_name: Name of the asset to get data of.
        :param asset_version: The asset version to get data of.
        :return: The asset data if it was found else None.
    """
    return local.get_asset_data(asset_name, asset_version) or remote.get_asset_data(
        asset_name, asset_version
    )


def get_version(asset_name: str, version_rule: str) -> Optional[str]:
    """Get the version of an asset for the given version rule.

    :param asset_name: Name of the asset to get a version of.
    :param version_rule: The version rule. eg '>=1.0.0'
    :return: The version matching the rule if it is found else None.
    """
    local_version = local.get_version(asset_name, version_rule)
    remote_version = remote.get_version(asset_name, version_rule)
    if local_version and remote_version:
        asset_version = (
            local_version if local_version > remote_version else remote_version
        )
    else:
        asset_version = remote_version or local_version
    return asset_version


def get_asset(asset_name: str, version: str) -> bytes:
    """Load a package to the local gam tmp directory.

    :param asset_name: Name of the asset to load.
    :param version: The version of the asset to load.
    :return: The package tarball.
    """
    package = local.get_asset(asset_name, version)
    if not package:
        package = remote.get_asset(asset_name, version)
    return package
