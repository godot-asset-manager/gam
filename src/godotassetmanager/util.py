import re
import tarfile
from pathlib import Path
from typing import Optional, Union

import toml
from packaging import version

from godotassetmanager.assetdata import AssetData

__all__ = (
    "get_asset_data_from_file",
    "get_name_and_version_rule",
    "get_use_version",
    "unzip_tar",
)
Version = Union[version.Version, version.LegacyVersion]


def get_asset_data_from_file(gam_filepath: Path) -> AssetData:
    """Get an AssetData object mapped from a gam.toml file.

    :param gam_filepath: Path to a gam.toml file.
    :return: An AssetData object mapped from the file at gam_filepath.
    """
    data = toml.load(gam_filepath)["gam"]
    if icon_name := data.get("icon"):
        with open(gam_filepath.parent.joinpath(icon_name), "b") as ico:
            icon = ico.read()
    else:
        icon = None
    return AssetData(
        title=data.get("title"),
        name=data.get("name"),
        author=data.get("author"),
        version=data.get("version"),
        description=data.get("description"),
        dependencies=data.get("dependencies") or [],
        tags=data.get("tags") or [],
        icon=icon,
    )


def get_name_and_version_rule(asset_str: str) -> tuple[str, Optional[str]]:
    """Get the name and version rule of an asset from a single line str.

    :param asset_str: A string containing the asset name and optionally
        the version rule. eg: 'my_asset>=1.0.0' -> 'my_asset', '>=1.0.0'
    :return: A tuple of the asset name and version rule.
    """
    try:
        match = re.search(r"^([\w][\w.]+)([=<>]{2}.*$)?", asset_str)
        name = match.group(1)
        version_rule = match.group(2)
        return name, version_rule
    except AttributeError:
        return asset_str, None


def unzip_tar(tar_path: Path, target_path: Path) -> None:
    """Unzip a tar.gz file into the target path.

    :param tar_path: Path to the tar file.
    :param target_path: Path to unzip into.
    :return: None
    """
    tar = tarfile.open(tar_path)
    tar.extractall(target_path)
    tar.close()


def get_use_version(
    versions: list[Version], version_rule: Optional[str]
) -> Optional[str]:
    """Get version to use from available versions and the version rule.

    :param versions: All available version.
    :param version_rule: The version rule to use to determine version.
    :return: The version to use if there is one, else None.
    """
    versions.sort()
    if not version_rule:
        return str(versions[-1])
    op, v = re.match(r"([=<>]+)(.*)", version_rule).groups()
    v = version.parse(v)
    if op == "==":
        asset_version = v if v in versions else None
    elif op == ">=":
        asset_version = versions[-1] if versions[-1] >= v else None
    elif op == "<=":
        versions = list(filter(lambda x: x <= v, versions))
        asset_version = versions[-1] if versions else None
    elif op == ">>":
        asset_version = versions[-1] if versions[-1] > v else None
    elif op == "<<":
        versions = list(filter(lambda x: x < v, versions))
        asset_version = versions[-1] if versions else None
    else:
        return None
    return str(asset_version) if asset_version else None
