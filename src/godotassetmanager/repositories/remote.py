import logging
import shutil
import tarfile
import uuid
from pathlib import Path
from typing import Optional

import requests
import toml
from packaging import version

from godotassetmanager import util
from godotassetmanager.assetdata import AssetData
from godotassetmanager.config import Config
from godotassetmanager.repositories import scraper

__all__ = (
    "get_asset_names",
    "get_asset",
    "download_from_url",
    "get_asset_data",
    "get_version",
)

log = logging.getLogger("gam")
config = Config.get_instance()


# TODO Add filter.
def get_asset_names() -> list[str]:
    """Get all repositories in the local repository.

    :return: A list of all local repositories.
    """
    # TODO
    pass


def get_asset(asset_name: str, asset_version: str) -> Optional[bytes]:
    """Get an asset of the given name and version.

    :param asset_name: The name of the asset to download.
    :param asset_version: The asset version to download.
    :return: The package if it is found else None.
    """
    for repo in config.repositories:
        pack_name = f"{asset_name}-{asset_version}.tar.gz"
        url = f"{repo}/{asset_name}/{asset_version}/{pack_name}"
        try:
            resp = requests.get(url)
            if resp.status_code == 200:
                return resp.content
        except requests.exceptions.ConnectionError:
            pass


def download_from_url(url: str) -> Optional[Path]:
    """Download a package from a URL into the gam tmp directory.

    :param url: URL to download a package from.
    :return: The path to the downloaded package.
    """
    log.info("Downloading from %s...", url)
    config.tmp_path.mkdir(exist_ok=True)
    dl_target = config.tmp_path.joinpath(f"{uuid.uuid4()}.tar.gz")
    resp = requests.get(url, stream=True)
    if resp.status_code != 200:
        log.warning("Download failed with message %s.", resp.content)
        return None
    with open(dl_target, "wb") as fd:
        for chunk in resp.iter_content(chunk_size=128):
            fd.write(chunk)
    with tarfile.open(dl_target) as tf:
        name = tf.getnames()[0]
        new_target = dl_target.parent.joinpath(f"{name}.tar.gz")
        shutil.move(dl_target, new_target)
    return new_target


def get_asset_data(asset_name: str, asset_version: str) -> Optional[AssetData]:
    """Get the asset data of an asset from a repository.

    :param asset_name: Name of the asset to get data of.
    :param asset_version: The asset version to get data of.
    :return: The asset data if it was found else None.
    """
    asset_data = None
    for repo in config.repositories:
        if asset_data:
            break
        asset_data = _get_repo_asset_data(repo, asset_name, asset_version)
    return asset_data


def get_version(asset_name: str, version_rule: Optional[str] = None) -> Optional[str]:
    """Get a package version for the given version rule.

    :param asset_name: Name of the asset to get a version of.
    :param version_rule: The version rule. eg '>=1.0.0'
    :return: The version matching the rule if it is found else None.
    """
    asset_version = None
    for repo in config.repositories:
        if asset_version:
            break
        try:
            resp = requests.get(f"{repo}/{asset_name}/")
            if resp.status_code == 200:
                versions = scraper.get_versions(resp.content)
                asset_version = util.get_use_version(
                    [version.parse(it) for it in versions], version_rule
                )
        except requests.exceptions.ConnectionError:
            pass
    if asset_version:
        log.debug("Found %s-%s.", asset_name, asset_version)
    return asset_version


def _get_repo_asset_data(
    repo: str, asset_name: str, asset_version: str
) -> Optional[AssetData]:
    try:
        url = f"{repo}/{asset_name}/{asset_version}/gam.toml"
        resp = requests.get(url)
        if resp.status_code == 200:
            dictionary = toml.load(resp.content.decode("utf-8"))
            return AssetData(**dictionary)
    except requests.exceptions.ConnectionError:
        return
