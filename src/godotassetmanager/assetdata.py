from dataclasses import dataclass, field
from typing import Optional


@dataclass
class AssetData:
    title: Optional[str] = None
    name: Optional[str] = None
    author: Optional[str] = None
    version: Optional[str] = None
    # TODO Description file.
    description: Optional[str] = None
    dependencies: [list[str]] = field(default_factory=list)
    tags: list[str] = field(default_factory=list)
    icon: Optional[bytes] = None
