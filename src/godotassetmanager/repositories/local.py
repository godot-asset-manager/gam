import logging
from typing import Optional

from packaging import version

from godotassetmanager import util
from godotassetmanager.assetdata import AssetData
from godotassetmanager.config import Config

log = logging.getLogger("gam")
config = Config.get_instance()


# TODO Add filter
def get_asset_names() -> list[str]:
    """Get all repositories in the local repository.

    :return: A list of all local repositories.
    """
    return [asset_dir.name for asset_dir in config.repo_path.iterdir()]


def get_asset(asset_name: str, asset_version: str) -> Optional[bytes]:
    """Get an asset of the given name and version.

    :param asset_name: The name of the asset to load.
    :param asset_version: The asset version to load.
    :return: The package if it is found else None.
    """
    asset_path = config.repo_path.joinpath(asset_name, asset_version)
    if asset_path.is_dir():
        package_path = asset_path.joinpath(f"{asset_name}-{asset_version}.tar.gz")
        return package_path.read_bytes()


def get_asset_data(asset_name: str, asset_version: str) -> Optional[AssetData]:
    """Get the asset data of an asset from the local repository.

    :param asset_name: Name of the asset to get data of.
    :param asset_version: The asset version to get data of.
    :return: The asset data if it was found else None.
    """
    try:
        return util.get_asset_data_from_file(
            config.repo_path.joinpath(asset_name, asset_version, "gam.toml")
        )
    except FileNotFoundError:
        return None


def get_version(asset_name: str, version_rule: Optional[str] = None) -> Optional[str]:
    """Get the version of a local asset for the given version rule.

    :param asset_name: Name of the asset to get a version of.
    :param version_rule: The version rule. eg '>=1.0.0'
    :return: The version matching the rule if it is found else None.
    """
    if not config.repo_path.joinpath(asset_name).is_dir():
        return None
    versions = [
        version.parse(p.name) for p in config.repo_path.joinpath(asset_name).iterdir()
    ]
    versions.sort()
    if not versions:
        return None
    if version_rule in map(str, versions):
        return version_rule
    if not version_rule:
        return str(versions[-1])
    return util.get_use_version(versions, version_rule)
