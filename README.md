[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)

# Godot Asset Manager

Godot asset manager (gam) is a CLI tool for packaging and installing
Godot assets.

## Installing GAM
There is no official installer at this time, and so far is only for
Linux. gam requires [Python 3.9](https://www.python.org/downloads/)

With Python 3.9 installed, clone this project and run:
```sh
bash ./build/install.sh
```

## Installing Assets
To install an asset, navigate to the root directory of a Godot project
and type the command:
```sh
gam install asset_name
```

To install all dependencies listed in a config file run the command:
```sh
gam install -d /path/to/gam.toml
```

gam will search any configured repositories for a package with that name
to install. All installed packages will be installed in `~/.gam/cache`,
then a symbolic link will be made in the project addons folder to the
system installed asset.

To install a local package.
```sh
gam install /path/to/my/asset_name.tar.gz
```

To install a package into a Godot project from a different directory
you can supply the target directory as an argument.
```sh
gam install asset_name /path/to/godot/project/root
```

## Building Assets

#### Packaging Rules
In order to work with gam, an asset needs to follow some rules.
- Asset names can only contain numbers, letters, periods, and
  underscores.
- Asset name cannot start with a period.
- Assets must have a `gam.toml` file in the project root.
- All the contents of an asset must be found in the project addons
  directory, eg `res://addons/myassetdirectory`. Nothing else in the
  project will be packaged by gam.

#### Config File
To prepare an asset to be packaged, create a `gam.toml` file in the
project root `res://`. This will include things like name, version,
dependencies, etc. To build a package the config file must include at
minimum a name and version.

The directory name of your asset in `res://addons/` must be the same as
the name given in the config file.

Example gam.toml file
```toml
[gam]
title = "Coffee Cup"
name = "coffee_cup"
author = "Coffea Arabica"
description = "A daily necessity."
version = "1.0.0"
dependencies = [
    "french.vanilla==1.0.0",  # Get package coffee of exactly this version.
    "mocha>=5.0.1",           # Get this version or greater.
    "peppermint>>0.0.7",      # Get a version greater than this.
    "vanilla"                 # Unspecified will get latest version.
]
```

## Repositories

### Add Asset Repository
To add a repository open the config file at `~/.gam/config.toml` and add
the url of the repository you wish to add.
```toml
[gam]
repositories = [
  "http://localhost:8080/"
]
```

### Host Asset Repository
To host a repository, first install the assets you wish to host on the
machine that will host them. Then navigate to `~/.gam/repo` and serve
the files with a python http server or twistd.

Python http module:
```sh
python -m http.server
```

twistd:
```sh
twistd -n web --path .
```
