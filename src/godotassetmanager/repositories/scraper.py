from lxml import html


def get_versions(html_bytes: bytes) -> list[str]:
    tree = html.fromstring(html_bytes)
    directories = tree.xpath("//a/text()")
    return [it[:-1] for it in directories]
