#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
VER=0.0.16
mkdir -p ~/.gam
mkdir -p ~/.gam/cache
mkdir -p ~/.gam/repo
mkdir -p ~/.local/bin
cp "${DIR}/../bin/gam" ~/.local/bin/gam
chmod +x ~/.local/bin/gam
python3.9 "${DIR}/../setup.py" sdist
python3.9 -m pip install "${DIR}/../dist/godot-asset-manager-${VER}.tar.gz"
