import os
import shutil
from pathlib import Path

from godotassetmanager.config import Config

os.environ["GAM_CONFIG"] = str(Path.cwd().joinpath("gam"))
config = Config.get_instance()
sample_path = Path.cwd().joinpath("samples")
mocha_path = sample_path.joinpath("mocha", "dist", "mocha-5.0.1.tar.gz")
vanilla_path = sample_path.joinpath(
    "french.vanilla", "dist", "french.vanilla-1.0.0.tar.gz"
)
installed_mocha = sample_path.joinpath("coffee", "addons", "mocha")
installed_vanilla = sample_path.joinpath("coffee", "addons", "french.vanilla")


def cleanup() -> None:
    shutil.rmtree(sample_path.joinpath("coffee", "addons"), ignore_errors=True)
    shutil.rmtree(sample_path.joinpath("mocha", "dist"), ignore_errors=True)
    shutil.rmtree(sample_path.joinpath("french.vanilla", "dist"), ignore_errors=True)
    mocha_path.is_file() and os.remove(mocha_path)
    vanilla_path.is_file() and os.remove(vanilla_path)
    shutil.rmtree(config.cache_path, ignore_errors=True)
    shutil.rmtree(config.repo_path, ignore_errors=True)


# Pre-test Cleanup #
cleanup()

# Setup #
config.repo_path.mkdir(exist_ok=True)
assert config.repo_path.is_dir()
config.cache_path.mkdir(exist_ok=True)

# Tests #
# Test building packages.
gam = "python3.9 ../bin/gam"
os.system(f"{gam} build ./samples/mocha/gam.toml")
os.system(f"{gam} build ./samples/french.vanilla/gam.toml")
assert mocha_path.is_file()
assert vanilla_path.is_file()

# Test installing packages to local repo.
os.system(f"{gam} install ./samples/french.vanilla/dist/french.vanilla-1.0.0.tar.gz")
os.system(f"{gam} install ./samples/mocha/dist/mocha-5.0.1.tar.gz")
assert vanilla_path.is_file()
assert vanilla_path.is_file()
assert config.cache_path.joinpath("mocha", "mocha-5.0.1", "gam.toml").is_file()
assert config.cache_path.joinpath(
    "french.vanilla", "french.vanilla-1.0.0", "gam.toml"
).is_file()
assert config.repo_path.joinpath("mocha", "5.0.1", "gam.toml").is_file()
assert config.repo_path.joinpath("french.vanilla", "1.0.0", "gam.toml").is_file()

# Test install by name.
os.system("cd ./samples/coffee && python3.9 ../../../bin/gam install mocha")
assert installed_mocha.is_dir()
# Test installing dependencies.
os.system("python3.9 ../bin/gam install -d ./samples/coffee/gam.toml")
assert installed_mocha.is_dir()
assert installed_vanilla.is_dir()

# Post-test Cleanup #
cleanup()
print("All integration tests completed successfully.")
