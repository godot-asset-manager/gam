import unittest
from packaging import version
from godotassetmanager import util


class UtilTest(unittest.TestCase):
    def test_get_name_and_version(self) -> None:
        # No version rule.
        name_version = "french.vanilla.coffee"
        name, ver = util.get_name_and_version_rule(name_version)
        self.assertEqual(name_version, name)
        self.assertIsNone(ver)
        # With version rule.
        ver = "==1.0.1"
        name_version = f"{name}{ver}"
        parsed_name, parsed_version = util.get_name_and_version_rule(name_version)
        self.assertEqual(name, parsed_name)
        self.assertEqual(ver, parsed_version)

    def test_get_use_version(self) -> None:
        versions = ["0.0.10", "0.0.9", "0.0.8"]
        versions = list(map(version.parse, versions))
        # Testing equality.
        rule = "==0.0.9"  # Exists
        ver = util.get_use_version(versions, rule)
        self.assertEqual("0.0.9", ver)
        rule = "==0.0.7"  # Less than least.
        ver = util.get_use_version(versions, rule)
        self.assertIsNone(ver)
        rule = "==0.0.11"  # Greater than greatest.
        ver = util.get_use_version(versions, rule)
        self.assertIsNone(ver)
        # Greater than or equals should return greatest if later than
        # target if there is a version later than target.
        rule = ">=0.0.8"
        ver = util.get_use_version(versions, rule)
        self.assertEqual("0.0.10", ver)
        # Greater than or equals should still work if target is less
        # than the lowest version.
        rule = ">=0.0.7"
        ver = util.get_use_version(versions, rule)
        self.assertEqual("0.0.10", ver)
        # Greater than or equals should return greatest if target is
        # greatest.
        rule = ">=0.0.10"
        ver = util.get_use_version(versions, rule)
        self.assertEqual("0.0.10", ver)
        # Greater than or equals should return None if target is greater
        # than greatest.
        rule = ">=0.0.11"
        ver = util.get_use_version(versions, rule)
        self.assertIsNone(ver)
        # Less than or equals should return target if target is the
        # greatest that is not greater than target.
        rule = "<=0.0.8"
        ver = util.get_use_version(versions, rule)
        self.assertEqual("0.0.8", ver)
        # Less than or equals should return None if target is less than
        # the lowest version.
        rule = "<=0.0.7"
        ver = util.get_use_version(versions, rule)
        self.assertIsNone(ver)
        # Less than or equals should return greatest if target is the
        # greatest.
        rule = "<=0.0.10"
        ver = util.get_use_version(versions, rule)
        self.assertEqual(ver, "0.0.10")
        # Less than or equals should return greatest if target is
        # greater
        # than the greatest.
        rule = "<=0.0.11"
        ver = util.get_use_version(versions, rule)
        self.assertEqual(ver, "0.0.10")
        # Greater than should return greatest if target is less than
        # the greatest.
        rule = ">>0.0.9"
        ver = util.get_use_version(versions, rule)
        self.assertEqual(ver, "0.0.10")
        rule = ">>0.0.6"
        ver = util.get_use_version(versions, rule)
        self.assertEqual(ver, "0.0.10")
        # Greater than should return None if target is the greatest.
        rule = ">>0.0.10"
        ver = util.get_use_version(versions, rule)
        self.assertIsNone(ver)
        # Greater than should return None if target greater than
        # greatest.
        rule = ">>0.0.11"
        ver = util.get_use_version(versions, rule)
        self.assertIsNone(ver)
        # Less than should return None if target is lowest or less than
        # lowest.
        rule = "<<0.0.8"
        ver = util.get_use_version(versions, rule)
        self.assertIsNone(ver)
        rule = "<<0.0.7"
        ver = util.get_use_version(versions, rule)
        self.assertIsNone(ver)
        # Less than should return greatest available versions that is
        # less than target.
        rule = "<<0.0.10"
        ver = util.get_use_version(versions, rule)
        self.assertEqual("0.0.9", ver)
        # Less than should return greatest if target greater than
        # greatest.
        rule = "<<0.0.11"
        ver = util.get_use_version(versions, rule)
        self.assertEqual("0.0.10", ver)
        # No rule should return greatest version.
        rule = None
        ver = util.get_use_version(versions, rule)
        self.assertEqual("0.0.10", ver)


if __name__ == "__main__":
    unittest.main()
