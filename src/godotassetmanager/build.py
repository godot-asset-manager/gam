import logging
import os
import re
import shutil
import tarfile
from pathlib import Path
from typing import Optional

from godotassetmanager.assetdata import AssetData
from godotassetmanager.install import installer
from godotassetmanager import util

log = logging.getLogger("gam")


def build(gam_filepath: Path, cache: Optional[bool] = None) -> None:
    """Build an asset into a distributable tarball.

    :param gam_filepath: Filepath to the gam.toml config file.
    :param cache: If True, install to local cache.
    """
    project_path = gam_filepath.parent
    config = util.get_asset_data_from_file(gam_filepath)
    log.info("Building %s-%s...", config.name, config.version)
    dist_path = project_path.joinpath("dist")
    dist_path.mkdir(exist_ok=True)
    package_path = dist_path.joinpath(f"{config.name}-{config.version}")
    # Remove any existing builds of this version.
    shutil.rmtree(package_path, ignore_errors=True)
    package_path.mkdir()
    plugin_cfg = project_path.joinpath("addons", config.name, "plugin.cfg")
    if plugin_cfg.is_file():
        log.debug("Updating plugin.cfg file.")
        _update_plugin_cfg(plugin_cfg, config)
    log.debug("Copying addons package to dist...")
    addon_path = project_path.joinpath("addons")
    for path in addon_path.joinpath(config.name).iterdir():
        if path.is_dir():
            shutil.copytree(path, package_path.joinpath(path.name))
        else:
            shutil.copy(path, package_path.joinpath(path.name))
    shutil.copy(gam_filepath, package_path.joinpath(gam_filepath.name))
    log.debug("Creating tarball...")
    package = str(dist_path.joinpath(f"{config.name}-{config.version}.tar.gz"))
    _make_tarfile(str(package_path), package)
    shutil.rmtree(package_path, ignore_errors=True)
    if cache:
        log.info("Installing built package to local cache...")
        installer.install(package)
    log.info("Finished building %s-%s successfully.", config.name, config.version)


def _make_tarfile(source_dir: str, output_filename: str) -> None:
    with tarfile.open(output_filename, "w:gz") as tar:
        tar.add(source_dir, arcname=os.path.basename(source_dir))


def _update_plugin_cfg(plugin_cfg_path: Path, asset_data: AssetData) -> None:
    with open(plugin_cfg_path, "r+") as f:
        content = f.read()
        for k, v in {
            "name": asset_data.title,
            "version": asset_data.version,
            "author": asset_data.author,
            "description": asset_data.description,
        }.items():
            content = re.sub(fr"({k}=)(.*)", f'{k}="{v}"', content)
        f.seek(0)
        f.truncate(0)
        f.write(content)
