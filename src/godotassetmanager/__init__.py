import logging

from chromaformatter import ChromaFormatter, Colors

formatter = ChromaFormatter(
    f"{Colors.Fore.GREEN}%(asctime)-0s"
    f" {Colors.Fore.MAGENTA}%(filename)s"
    f": %(lineno)-3s - {Colors.LEVEL_COLOR}%(message)s"
)
log = logging.getLogger("gam")
log.setLevel(logging.DEBUG)

stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
log.addHandler(stream_handler)
