import os
from pathlib import Path

import toml


class Config:
    __instance__: "Config" = None

    def __init__(self) -> None:
        if Config.__instance__ is not None:
            raise Exception("Cannot create multiple instances of a singleton")
        else:
            Config.__instance__ = self
        if path := os.environ.get("GAM_CONFIG"):
            gam_path = Path(path)
        else:
            gam_path = Path.home().joinpath(".gam")
        config_path = gam_path.joinpath("config.toml")
        try:
            data = toml.load(config_path).get("gam")
            repos = [it.removesuffix("/") for it in data.get("repositories")]
            self.repositories: list[str] = repos or []
        except FileNotFoundError:
            self.repositories: list[str] = []
            config_path.touch()
            toml.dump({"repositories": self.repositories}, config_path)
        self.tmp_path: Path = gam_path.joinpath(Path("tmp"))
        self.cache_path: Path = gam_path.joinpath(Path("cache"))
        self.repo_path: Path = gam_path.joinpath(Path("repo"))

    @staticmethod
    def get_instance() -> "Config":
        if Config.__instance__ is None:
            return Config()
        else:
            return Config.__instance__
