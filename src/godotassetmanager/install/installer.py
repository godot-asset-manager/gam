import logging
import os
import re
import shutil
from pathlib import Path
from typing import Optional

from godotassetmanager import util
from godotassetmanager.assetdata import AssetData
from godotassetmanager.config import Config
from godotassetmanager.repositories import remote, repos_interface as ri

__all__ = ("install", "install_dependencies")

log = logging.getLogger("gam")
config = Config.get_instance()


def install_dependencies(gam_filepath_str: str) -> None:
    """Install the dependencies listed in a gam config file.

    :param gam_filepath_str: Path to the gam config file.
    :return: None
    """
    project_path = Path(gam_filepath_str).absolute().parent
    asset_data = util.get_asset_data_from_file(Path(gam_filepath_str))
    log.info("Installing dependencies for %s.", asset_data.name)
    _install_dependencies(asset_data, project_path)


def install(
        target: str, version_rule: Optional[str] = None,
        project_path: Optional[Path] = None
) -> None:
    """Install an asset.

    :param target: The asset name, url, or filepath of an asset.
    :param version_rule: The version rule. eg '>=1.0.0'
    :param project_path: Project to install this asset into.
    :return: None
    """
    if version_rule:
        log.debug("Install target %s%s.", target, version_rule)
    else:
        log.debug("Install target %s.", target)
    shutil.rmtree(config.tmp_path, ignore_errors=True)
    asset_data: Optional[AssetData] = None
    # If install target is a url.
    if bool(re.search(r"^https?://", target)):
        path = remote.download_from_url(target)
        if path:
            asset_data = _install_tmp_tarball_to_repo(path)
    elif Path(target).is_file():
        path = _load_from_path(Path(target))
        if path:
            asset_data = _install_tmp_tarball_to_repo(path)
    elif Path.cwd().joinpath(target).is_file():
        path = _load_from_path(Path(f"{os.getcwd()}/{target}"))
        if path:
            asset_data = _install_tmp_tarball_to_repo(path)
    # If target is not a URL or a filepath hit repositories.
    else:
        asset_version = ri.get_version(target, version_rule)
        if asset_version:
            asset_data = ri.get_asset_data(target, asset_version)
    if not asset_data:
        if version_rule:
            log.error("Failed to find target %s%s.", target, version_rule)
        else:
            log.error("Failed to find target %s.", target)
        return
    if not config.repo_path.joinpath(asset_data.name, asset_data.version).is_file():
        _install_to_local_repo(
            ri.get_asset(asset_data.name, asset_data.version), asset_data
        )
    log.info("Installing asset %s.", asset_data.name)
    _install_asset(asset_data, project_path)
    # Install dependencies.
    _install_dependencies(asset_data, project_path)
    shutil.rmtree(config.tmp_path, ignore_errors=True)


def _install_dependencies(
        asset_data: AssetData, project_path: Optional[Path] = None
) -> None:
    dependencies = ri.get_dependencies_data(asset_data)
    if not dependencies:
        log.info("No dependencies found to install for %s.", asset_data.name)
        return
    log.info("Installing:")
    [log.info("\t- %s-%s", d.name, d.version) for d in dependencies]
    for asset_data in dependencies:
        asset = ri.get_asset(asset_data.name, asset_data.version)
        _install_to_local_repo(asset, asset_data)
        _install_asset(asset_data, project_path)


def _install_asset(asset_data: AssetData, project_path: Optional[Path] = None) -> None:
    # Write the package into local repo.
    asset_repo_path = config.repo_path.joinpath(asset_data.name, asset_data.version)
    asset_repo_path.mkdir(exist_ok=True)
    repo_tarball = asset_repo_path.joinpath(
        f"{asset_data.name}-{asset_data.version}.tar.gz"
    )
    # Unzip path into cache path.
    asset_cache_path = config.cache_path.joinpath(asset_data.name)
    asset_cache_path.mkdir(exist_ok=True)
    shutil.rmtree(asset_cache_path.joinpath(asset_data.version), ignore_errors=True)
    util.unzip_tar(repo_tarball, asset_cache_path)
    # Copy the asset data file to local repo.
    shutil.copy(
        asset_cache_path.joinpath(
            f"{asset_data.name}-{asset_data.version}", "gam.toml"
        ),
        asset_repo_path.joinpath("gam.toml"),
    )
    # Link to cache and log success.
    success = True
    if project_path:
        success = _link(project_path, asset_data.name, asset_data.version)
    if success:
        log.info("Successfully installed %s-%s.", asset_data.name, asset_data.version)


def _install_to_local_repo(asset: bytes, asset_data: AssetData) -> None:
    # Write the package into local repo.
    config.repo_path.joinpath(asset_data.name).mkdir(exist_ok=True)
    asset_repo_path = config.repo_path.joinpath(asset_data.name, asset_data.version)
    asset_repo_path.mkdir(exist_ok=True)
    repo_tarball = asset_repo_path.joinpath(
        f"{asset_data.name}-{asset_data.version}.tar.gz"
    )
    shutil.rmtree(repo_tarball, ignore_errors=True)
    repo_tarball.write_bytes(asset)


def _install_tmp_tarball_to_repo(tarball_path: Path) -> Optional[AssetData]:
    log.debug("Installing from %s.", tarball_path)
    extract_path = tarball_path.parent
    util.unzip_tar(tarball_path, extract_path)
    ad = util.get_asset_data_from_file(
        extract_path.joinpath(tarball_path.name.removesuffix(".tar.gz"), "gam.toml")
    )
    _install_to_local_repo(tarball_path.read_bytes(), ad)
    return ad


def _load_from_path(package_path: Path) -> Path:
    log.info("Loading local package %s...", package_path.name)
    config.tmp_path.mkdir(exist_ok=True)
    shutil.copy(package_path, config.tmp_path.joinpath(package_path.name))
    return config.tmp_path.joinpath(package_path.name)


def _protect(protect_path: Path) -> None:
    for path, directories, files in os.walk(protect_path):
        [os.chmod(Path(f"{path}/{f}"), 0o544) for f in files]
        [_protect(Path(f"{path}/{d}")) for d in directories]


def _link(project_path: Path, name: str, version: str) -> bool:
    # If this project is also the one being installed, don't link.
    if (gam_path := project_path.joinpath("gam.toml")).is_file():
        project_data = util.get_asset_data_from_file(gam_path)
        if name == project_data.name:
            return True
    project_addons_path = project_path.joinpath("addons")
    project_addons_path.mkdir(exist_ok=True)
    package_path = str(config.cache_path.joinpath(name, f"{name}-{version}"))
    if not Path(package_path).is_dir():
        log.error("%s is not installed on this system and cannot be linked.", name)
        return False
    install_path = project_addons_path.joinpath(name)
    try:
        install_path.unlink()
    except IsADirectoryError:
        log.error("Can't unlink directory %s.", install_path)
        return False
    except FileNotFoundError:
        pass
    log.debug("Creating symbolic link for %s.", name)
    os.symlink(package_path, install_path, target_is_directory=True)
    return True
